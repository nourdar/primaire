@section('courseList')



{{--Courses List--}}
<div class="container">

    {{--<h1 class="text-center">{{ $situation->title }}</h1>--}}

    <hr>
@if(count($courses) > 0)
    {{--Start Course--}}

    @foreach($courses as $course)

        <div class="course text-right col-lg-12 col-md-12 col-sm12 col-xs-12">
            <div class="course-desc text-right col-lg-8  ">
                <h2> {{ $course->title }}</h2>
                <h4>{{ $course->description }}</h4>
                <br>
                    @if($course->access == 1)
                    <button class="btn btn-primary btn-wide"  >
                        <a href="{{ url('activity/show/'.$course->id ) }}"> الدخول إلى الدرس</a>
                    </button>
                    @else
                        <div class="alert alert-warning">
                            لا يمكنك الدخول إلى هذا الدرس حتى تكمل الدرس الذي قبله
                        </div>
                    @endif

            </div>

            <div class="course-img col-lg-3">
                <img class="img-thumbnail" src={{ url("img/".$course->image) }} width="100" height="100">
            </div>

        </div>
        {{--separator--}}
            <div class="hr"></div>
        {{--end separator--}}
    @endforeach
    {{--End Course--}}
@else
    <div class="alert alert-warning text-right">لا يوجد دروس في هذه الوضعية</div>
@endif

</div>
{{--End Courses List--}}

@endsection
