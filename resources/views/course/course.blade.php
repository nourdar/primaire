
{{--include blade files--}}
@include('head')
@include('course.menu')

@include($page)


@include('footer')




{{--sections that will appear--}}
@yield('head')
@yield('menu')

{{--seperator--}}
<div class="seperation-top "></div>
{{--end seperator--}}

@yield($section)


@yield('footer')
