@section('editActivity')




    <div class="container">



        @if(session('success'))
            <div class="alert alert-success">تم تعديل النشاط بنجاح</div>
        @endif
        @if(session('false'))
            <div class="alert alert-danger">لم يتم تعديل النشاط بنجاح .</div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="form-horizontal text-right " method="post" action="{{ url('admin/activity/edit/update') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ $activity['id'] }}">
            <input type="hidden" name="is_done" value="{{ $activity['is_done'] }}">
            <fieldset>

                <!-- Form Name -->
                <legend>تعديل نشاط</legend>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class=" control-label" for="selectbasic">الدرس</label>
                    <div >
                        <select id="selectbasic" name="parent_id" class="form-control">
                                <option value="{{ $course['id'] }}">{{ $course['title'] }}</option>
                        </select>
                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class=" control-label" for="selectbasic">نوع النشاط</label>
                    <div >
                        <select id="selectbasic" name="type" class="form-control">
                            <option @if($activity['type'] == "أكتشف") selected @endif value="أكتشف">أكتشف</option>
                            <option @if($activity['type'] == "أنجز") selected @endif value="أنجز">أنجز</option>
                            <option @if($activity['type'] == "تعلمت") selected @endif value="تعلمت">تعلمت</option>
                        </select>
                    </div>
                </div>


                <!-- Textarea -->
                <div class="form-group">
                    <label  for="description">مدة النشاط ( بالدقائق )</label>
                    <div >
                        <input type="number" name="duration"  value="{{ $activity['duration'] }}"/>
                    </div>
                </div>
                <!-- Textarea -->
                <div class="form-group">
                    <label  for="description">وصف النشاط</label>
                    <div >
                        <textarea class="form-control" id="description" name="description">
                            {{ $activity['description'] }}</textarea>
                    </div>
                </div>

                <!-- File Button -->
                <div class="form-group">
                    <label class=" control-label" for="image">الصورة</label>
                    <div >
                        <img src="{{ asset('img/'.$activity['image']) }}" width="100" height="100">
                        <input id="image" name="image" class="input-file" type="file">
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class=" control-label" for="submit"></label>
                    <div >
                        <button id="submit" name="submit" type="submit" class="btn btn-primary">تعديل النشاط</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>



@endsection
