@section('activitiesList')



    {{--activitys List--}}
    <div class="container">
        @if(count($activities) > 0)
            {{--Start activity--}}

            @if(session('success'))
                <div class="alert alert-success">تمت العملية بنجاح</div>
            @endif
            @if(session('false'))
                <div class="alert alert-danger">لم تتم العملية بنجاح</div>
            @endif

            @foreach($activities as $activity)
                <div class="course text-right col-lg-12 col-md-12 col-sm12 col-xs-12">

                    <div class="course-desc text-right col-lg-8  ">
                        <h4><a href="{{ url('activity/'.$activity->id) }}"> ( {{$activity->type}} ) {{ $activity->description }} </a></h4>
                        <br>
                        <br>
                        <a href="{{ url('admin/activity/edit/'.$activity->id) }}"><button class="btn btn-dark">تعديل</button></a>
                        <form class="d-inline-block" action="{{ url('admin/activity/delete') }}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ $activity->id }}">
                            <a><button class="btn btn-danger" type="submit">حذف</button></a>
                        </form>
                    </div>

                    <div class="course-img col-lg-3">
                        <img class="img-thumbnail" src={{ url("img/".$activity->image) }} width="100" height="100">
                    </div>
                </div>
                {{--separator--}}
                <div class="hr"></div>
                {{--end separator--}}
            @endforeach
            {{--End activity--}}
        @else
            <div class="alert alert-warning text-right">لا يوجد نشاطات في هذا الدرس</div>
        @endif

    </div>
    {{--End activitys List--}}

@endsection

