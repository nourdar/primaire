@section('menu')

    <!-- Navigation -->
    <nav class="navbar admin-nav navbar-expand-lg navbar-light fixed-top text-right" id="mainNav">
        <div class="container ">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">التحكم في الوضعيات
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('admin/situation/show') }}" class="nav-link">عرض الوضعيات</a></li>
                            <li><a href="{{ url('admin/situation/add') }}" class="nav-link">إضافة وضعية</a></li>
                        </ul>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">التحكم في الدروس
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('admin/situation/show') }}" class="nav-link">عرض الدروس</a></li>
                            <li><a href="{{ url('admin/course/add') }}" class="nav-link">إضافة درس</a></li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/') }}">رئيسية الموقع</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<div class="seperation-bottom"></div>
@endsection
