@section('editCourse')
<div class="container">
    @if(session('success'))
        <div class="alert alert-success">تم تعديل الدرس بنجاح</div>
    @endif
    @if(session('false'))
        <div class="alert alert-danger">لم يتم تعديل الدرس بنجاح .</div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form class="form-horizontal text-right " method="post" action="{{ url('admin/course/edit/update') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $course->id }}">
        <input type="hidden" name="is_done" value="{{ $course->is_done }}">
        <fieldset>

            <!-- Form Name -->
            <legend>تعديل الدرس</legend>

            <!-- Select Basic -->
            <div class="form-group">
                <label class=" control-label" for="selectbasic">الوضعية</label>
                <div >
                    <select id="selectbasic" name="parent_id" class="form-control">
                        @foreach($situation as $key)
                            <option @if($key['id'] == $course->parent_id) selected @endif value="{{ $key['id'] }}">{{ $key['title'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class=" control-label" for="title">عنوان الدرس</label>
                <div >
                    <input id="title" name="title" type="text" value="{{ $course->title }}" class="form-control input-md">

                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label  for="description">وصف الدرس</label>
                <div >
                    <textarea class="form-control" id="description" name="description"> {{ $course->description }} </textarea>
                </div>
            </div>

            <!-- File Button -->
            <div class="form-group">
                <label class=" control-label" for="image">الصورة</label>
                <div >
                    <img src="{{ asset('img/'.$course->image) }}" width="100" height="100">
                    <input id="image" name="image" class="input-file" type="file">
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class=" control-label" for="submit"></label>
                <div >
                    <button id="submit" name="submit" type="submit" class="btn btn-primary">تعديل الدرس</button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
@endsection
