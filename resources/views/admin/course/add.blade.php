@section('addCourse')
<div class="container">
    @if(session('success'))
        <div class="alert alert-success">تم إضافة الدرس بنجاح</div>
    @endif
    @if(session('false'))
        <div class="alert alert-danger">لم يتم إضافة الدرس بنجاح .</div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form class="form-horizontal text-right " method="post" action="{{ url('admin/course/submitAdd') }}" enctype="multipart/form-data">
        @csrf
        <fieldset>

            <!-- Form Name -->
            <legend>إضافة درس</legend>

            <!-- Select Basic -->
            <div class="form-group">
                <label class=" control-label" for="selectbasic">الوضعية</label>
                <div >
                    <select id="selectbasic" name="parent_id" class="form-control">
                        @foreach($situations as $situation)
                            <option
                                @if($current['id'] == $situation['id']) selected @endif
                                value="{{ $situation['id'] }}">{{ $situation['title'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class=" control-label" for="title">عنوان الدرس</label>
                <div >
                    <input id="title" name="title" type="text" placeholder="" class="form-control input-md">

                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label  for="description">وصف الدرس</label>
                <div >
                    <textarea class="form-control" id="description" name="description"></textarea>
                </div>
            </div>

            <!-- File Button -->
            <div class="form-group">
                <label class=" control-label" for="image">الصورة</label>
                <div >
                    <input id="image" name="image" class="input-file" type="file">
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class=" control-label" for="submit"></label>
                <div >
                    <button id="submit" name="submit" type="submit" class="btn btn-primary">إضافة الدرس</button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
@endsection
