@section('courseList')



    {{--Courses List--}}
    <div class="container">
        @if(count($courses) > 0)
            {{--Start Course--}}

            @if(session('success'))
                <div class="alert alert-success">تمت العملية بنجاح</div>
            @endif
            @if(session('false'))
                <div class="alert alert-danger">لم تتم العملية بنجاح</div>
            @endif

            @foreach($courses as $course)
                <div class="course text-right col-lg-12 col-md-12 col-sm12 col-xs-12">

                    <div class="course-desc text-right col-lg-8  ">
                        <h2><a href="{{ url('admin/activity/show/'.$course->id) }}">{{ $course->title }}</a></h2>
                        <h4>{{ $course->description }}</h4>
                        <a href="{{ url('admin/activity/add/'.$course->id) }}"><button class="btn btn-primary">إضافة نشاط</button></a>
                        <a href="{{ url('admin/course/edit/'.$course->id) }}"><button class="btn btn-dark">تعديل</button></a>
                        <form class="d-inline-block" action="{{ url('admin/course/delete') }}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ $course->id }}">
                            <a><button class="btn btn-danger" type="submit">حذف</button></a>
                        </form>
                    </div>

                    <div class="course-img col-lg-3">
                        <img class="img-thumbnail" src={{ url("img/".$course->image) }} width="100" height="100">
                    </div>
                </div>
                {{--separator--}}
                <div class="hr"></div>
                {{--end separator--}}
            @endforeach
            {{--End Course--}}
        @else
            <div class="alert alert-warning text-right">لا يوجد دروس في هذه الوضعية</div>
        @endif

    </div>
    {{--End Courses List--}}

@endsection
