@section('editSituation')
    <div class="container">
        @if(session('success'))
            <div class="alert alert-success ">تم تعديل الوضعية بنجاح</div>
        @endif
            @if(session('false'))
                <div class="alert alert-danger ">لم يتم تعديل الوضعية بنجاح .</div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger ">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <form class="form-horizontal text-right " method="post" action="{{ url('admin/situation/update') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ $situation->id }}">
            <input type="hidden" name="is_done" value="{{ $situation->is_done }}">
            <fieldset>

                <!-- Form Name -->
                <legend>تعديل الوضعية</legend>



                <!-- Text input-->
                <div class="form-group">
                    <label class=" control-label" for="title">عنوان الوضعية</label>
                    <div >
                        <input id="title" name="title" type="text" value="{{ $situation->title }}" class="form-control input-md">
                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label  for="description">وصف الوضعية</label>
                    <div >
                        <textarea class="form-control" id="description" name="description">{{ $situation->description }}</textarea>
                    </div>
                </div>

                <!-- File Button -->
                <div class="form-group">
                    <label class=" control-label" for="image">الصورة</label>
                    <div >
                        <img src="{{ asset('img/'.$situation->image) }}" width="100" height="100">
                        <input id="image" name="image" value="{{ $situation->image }}" class="input-file" type="file">
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class=" control-label" for="submit"></label>
                    <div >
                        <button id="submit" name="submit" type="submit" class="btn btn-primary">تعديل الوضعية</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
@endsection
