@section('addSituation')
    <div class="container">
        @if(session('success'))
            <div class="alert alert-success ">تم إضافة الوضعية بنجاح</div>
        @endif
            @if(session('false'))
                <div class="alert alert-danger ">لم يتم إضافة الوضعية بنجاح .</div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger ">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <form class="form-horizontal text-right " method="post" action="{{ url('admin/situation/submitAdd') }}" enctype="multipart/form-data">
            @csrf
            <fieldset>

                <!-- Form Name -->
                <legend>إضافة وضعية</legend>



                <!-- Text input-->
                <div class="form-group">
                    <label class=" control-label" for="title">عنوان الوضعية</label>
                    <div >
                        <input id="title" name="title" type="text" placeholder="" class="form-control input-md">

                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label  for="description">وصف الوضعية</label>
                    <div >
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>
                </div>

                <!-- File Button -->
                <div class="form-group">
                    <label class=" control-label" for="image">الصورة</label>
                    <div >
                        <input id="image" name="image" class="input-file" type="file">
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class=" control-label" for="submit"></label>
                    <div >
                        <button id="submit" name="submit" type="submit" class="btn btn-primary">إضافة الوضعية</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
@endsection
