@section('situations')
    <!-- Projects Section -->
    <section id="projects" class="projects-section bg-light ">
        <div class="container">
            @if(session('success'))
                <div class="alert alert-success">تمت العملية بنجاح</div>
            @endif
            @if(session('false'))
                <div class="alert alert-danger">لم تتم العملية بنجاح</div>
            @endif

        @foreach($situations as $situation)
            <!-- Situation  Row -->

                <div class="row justify-content-center no-gutters mb-5 mb-lg-0 situation" >
                    <input type="hidden" name="situationId" value="{{ $situation['id'] }}">
                    <div class="col-lg-5 situation-img" >
                        <img class="img-fluid" src="{{ url('img/'.$situation['image']) }}" >
                    </div>
                    <div class="col-lg-7">
                        <div class="bg-black text-center h-100 project">
                            <div class="d-flex h-100">
                                <div class="project-text w-100 my-auto text-right {{--text-lg-left--}}">
                                    <h4 class="text-white"><a href="{{ url('admin/course/'.$situation['id']) }}">{{ $situation['title'] }}</a></h4>
                                    <p class="mb-0 text-white-50">{{ $situation['description'] }}
                                        <br>
                                    <form class="d-inline-block" action="{{ url('admin/situation/delete') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $situation->id }}">
                                        <a><button class="btn btn-danger" type="submit">حذف</button></a>
                                    </form>
                                        <a href="{{ url('admin/course/add/'.$situation->id) }}"><button class="btn btn-primary">إضافة درس</button></a>
                                        <a href="{{ url('admin/situation/edit/'.$situation->id) }}"><button class="btn btn-dark">تعديل </button></a>

                                    </p>
                                    <hr class="d-none d-lg-block mb-0 mr-0 ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Situation  Row -->
            @endforeach
        </div>
    </section>

@endsection
