@include('head')
@include('admin.menu')
@include('progress')
@include('footer')

<div class="seperation-top"></div>
<br>

<div class="alert alert-success p-4">
    <h2 class="text-center">مرحبا بك في لوحة التحكم</h2>
    <p>:
        بإمكانك من خلال هذه اللوحة أن تقوم بعدة عمليات منها


        <br>

        إضافة وضعيات والتعديل عليها وحذفها -
        <br>
         إضافة دروس والتعديل عليها وحذفها -
        <br>

        إضافة نشاطات والتعديل عليها وحذفها -
        <br>


    </p>
</div>

<div class="alert alert-info p-4">
    <h2 class="text-center">
        كيف أقوم بإضافة وضعية
        <i class="fa fa-info-circle"></i>
    </h2>
    <p class="text-center"><b>.
        إظغط على التحكم في الوضعيات ثم إظغط على إضافة وضعية
        </b>
    </p>
</div>


<div class="alert alert-info p-4">
    <h2 class="text-center">
        كيف أقوم بإضافة درس
        <i class="fa fa-info-circle"></i>
    </h2>
    <p class="text-center"><b>.
إظغط على التحكم في الوضعيات ثم إختر الوضعية التي ستضيف عليها الدرس ثم إضغط على زر إضافة درس        </b>
    </p>
</div>

<div class="alert alert-info p-4">
    <h2 class="text-center">
        كيف أقوم بإضافة نشاط
        <i class="fa fa-info-circle"></i>
    </h2>
    <p class="text-center"><b>.
            إذهب إلى التحكم في الدروس ثم عرض الدروس ثم قم بإختيار الدرس ثم إظغط على زر إضافة نشاط
                </b>
    </p>
</div>

<div class="seperation-bottom"></div>
@yield('head')
@yield('menu')
@yield('footer')
