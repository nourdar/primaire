<div class="container">
    <div class="seperation-top"></div>
    <br>
    <h1 class="text-right">:
        نسبة التقدم في النشاطات هو
    </h1>
    <div class="progress">

        <div class="progress-bar" role="progressbar" aria-valuenow="70"
             aria-valuemin="0" aria-valuemax="100"
             style="width:@if(session('progress')){{ session('progress') }} @else 0 @endif">
            <span class="sr-only">70% Complete</span>
        </div>
    </div>
</div>
