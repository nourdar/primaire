@section('activitiesList')

    <h4 class="text-center"><button  class="btn btn-primary"><a href="{{ url('course/'.$course->parent_id) }}">العودة إلى الدروس</a></button> </h4>
    <br>

    {{--activitys List--}}
    <div class="container">
        <h1 class="text-center"><a href="{{ url('course/'.$course->parent_id) }}">{{ $course->title }}</a> </h1>
        <hr>
        @if(count($activities) > 0)

            {{--Start activity--}}
            @foreach($activities as $activity)
                <div class="course text-right col-lg-12 col-md-12 col-sm12 col-xs-12">
                    <div class="course-desc text-right col-lg-8  ">
                        <h3> ( {{ $activity->type }} )</h3>
                        <h4> {{ $activity->description }}</h4>

                        @if($activity->access == 1 )
                            <button class="btn btn-primary btn-wide"  >
                                <a href="{{ url('activity/'.$activity->id ) }}"> الدخول إلى النشاط</a>
                            </button>
                        @else
                            <div class="alert alert-warning">
                                لا يمكنك الدخول إلى هذا النشاط حتى تكمل النشاط الذي قبله
                            </div>
                        @endif
                    </div>

                    <div class="course-img col-lg-3">
                        <img class="img-thumbnail" src={{ url("img/".$activity->image) }} width="100" height="100">
                    </div>
                </div>
                {{--separator--}}
                <div class="hr"></div>
                {{--end separator--}}

                @endforeach
            {{--End activity--}}
        @else
            <div class="alert alert-warning text-right">لا يوجد نشاطات في هذا الدرس</div>
        @endif

    </div>
    {{--End activitys List--}}

@endsection

