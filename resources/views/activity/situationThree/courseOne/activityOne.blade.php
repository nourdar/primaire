
@if(session('solution'))
    <div id="active">
        <h2 class="text-right mt-5">ما هي نتيجة كل من ( 5 + 13 ) و ( 6 - 9 ) ؟ </h2>
        <h4 class="text-right">يستعمل أمين للتنقل الشريط العددي</h4>
        <div class="block block-330 float-left">


            <h4 class="d-inline-block text-right">9 - 6</h4>
            <img src="{{ url('img/jam3.png') }}" alt="" width="100%">

            <table class="table " style="width: 120%">
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>2</td>
                    <td class="green">3</td>
                    <td>4</td>
                    <td>5</td>
                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                    <td>9</td>
                    <td>10</td>
                </tr>
            </table>
            <img src="{{ url('img/jam3-arrow.png') }}" alt="" style="width: 70%; margin: -16px -15px 0px 0px">
        </div>
        <div class="vertical-separation"></div>
        <div class="block block-330 ml-5">
            <h4 class="d-inline-block text-right">13 + 5</h4>
            <br>
            <div class="seperation-top"></div>
            <br>
            <img src="{{ url('img/tar7-arrow.png') }}" width="70%" style="margin-right: 35px">

            <table class="table ">
                <tr>
                    <td>12</td>
                    <td>13</td>
                    <td>14</td>
                    <td>15</td>
                    <td>16</td>
                    <td>17</td>
                    <td class="green">18</td>
                    <td>19</td>
                    <td>20</td>

                </tr>
            </table>
            <img src="{{ url('img/tar7.png') }}" alt="" style="width: 70%; margin: -16px 30px 0px 0px">
        </div>
        <h2 class="text-right mt-5">أكمل الحساب </h2>

        <div style="width: 800px; height: 100px; border: 1px solid black; margin: 0px auto; padding:5px">
            <table class="float-left" >
                <tr>
                    <td><b>9 - 6 = </b></td>
                    <td><b>3</b></td>
                </tr>
            </table>
            <table class="float-lg-right" >
                <tr>
                    <td><b>13 + 5 = </b></td>
                    <td><b>18</b></td>
                </tr>
            </table>
        </div>
    </div>
@else
    <div id="active">
        <h2 class="text-right mt-5">ما هي نتيجة كل من ( 5 + 13 ) و ( 6 - 9 ) ؟ </h2>
        <h4 class="text-right">يستعمل أمين للتنقل الشريط العددي</h4>
        <div class="block block-330 float-left">


            <h4 class="d-inline-block text-right">9 - 6</h4>
            <img src="{{ url('img/jam3.png') }}" alt="" width="100%">

            <table class="table " style="width: 120%">
                <tr>
                    <td>0</td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                    <td>9</td>
                    <td>10</td>
                </tr>
            </table>
            <img src="{{ url('img/jam3-arrow.png') }}" alt="" style="width: 70%; margin: -16px -15px 0px 0px">
        </div>
        <div class="vertical-separation"></div>
        <div class="block block-330 ml-5">
            <h4 class="d-inline-block text-right">13 + 5</h4>
            <br>
            <div class="seperation-top"></div>
            <br>
            <img src="{{ url('img/tar7-arrow.png') }}" width="70%" style="margin-right: 35px">

            <table class="table ">
                <tr>
                    <td>12</td>
                    <td>13</td>
                    <td>14</td>
                    <td>15</td>
                    <td>16</td>
                    <td>17</td>
                    <td>18</td>
                    <td>19</td>
                    <td>20</td>

                </tr>
            </table>
            <img src="{{ url('img/tar7.png') }}" alt="" style="width: 70%; margin: -16px 30px 0px 0px">
        </div>
        <h2 class="text-right mt-5">أكمل الحساب </h2>

        <div style="width: 800px; height: 100px; border: 1px solid black; margin: 0px auto; padding:5px">
            <table class="float-left" >
                <tr>
                    <td><b>9 - 6 = </b></td>
                    <td><input type="text" class="block-50 block"></td>
                </tr>
            </table>
            <table class="float-lg-right" >
                <tr>
                    <td><b>13 + 5 = </b></td>
                    <td><input type="text" class="block-50 block"></td>
                </tr>
            </table>
        </div>
    </div>
@endif

