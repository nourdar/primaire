@section('top')
    @isset($activity)
        <div class="seperation-top"></div>
        <div class="container">

            <h4 class="text-center"><button  class="btn btn-primary"><a href="{{ url('activity/show/'.$activity->parent_id) }}">العودة إلى الدرس</a></button> </h4>
            <br>
            <h1 class="text-center">{{ $activity->description }}</h1>
            @if(!session('solution'))
            <h4 class="text-right" id="time" data-time="{{$activity->duration}}">مدة النشاط : {{ $activity->duration }} دقيقة</h4>
             <h4 class="text-right" id="time">  الزمن المتبقي : <span id="time-display"></span></h4>
            @endif
            <div class="activity" id="activity">
                <form method="post" action="{{ url('activity/post/submit') }}" >
        @csrf
                    <input type="hidden" name="id" value="{{ $activity->id }}">
                    <input type="hidden" name="parent_id" value="{{ $activity->parent_id }}">
                    <h4 class="activity-label">{{ $activity->type }}</h4>
                    @if(session('solution'))

                    <h2>الحل هو كالآتي </h2>
                     @endif

    @endisset
@endsection
