{{--@include('head')--}}
{{--@include('admin.menu')--}}
{{--@include('footer')--}}



{{--@yield('head')--}}
{{--@yield('menu')--}}
{{--@yield('footer')--}}




{{--include blade files--}}
@include('head')
@include('nav')

@include('activity.top')
@yield('top')
@include($page)

@include('activity.bottom')

@isset($showBottom)

    @if(!$showBottom == true)

        @else
            @yield('bottom')
    @endif
@else
    @yield('bottom')
@endisset

@include('footer')




{{--sections that will appear--}}
@yield('head')
@yield('nav')

{{--seperator--}}
<div class="seperation-top "></div>
{{--end seperator--}}

@yield($section)


@yield('footer')
