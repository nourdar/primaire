@if(session('solution'))
    <div id="active">




        <div class="animals-activity">
            <img src="{{ url('img/animals-activity.png') }}" alt="">
        </div>
        <div class="table-numbers">
            <h2 class="text-right mt-4">ما هو عدد الحيوانات في المزرعة ؟</h2>
            <h2 class="text-right">عين العدد على الجدول</h2>
            <table class="table table-hover">
                <tr>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                </tr>
                <tr>

                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                    <td>9</td>
                    <td>10</td>
                </tr>
                <tr>


                    <td>11</td>
                    <td>12</td>
                    <td class="green">13</td>
                    <td>14</td>
                    <td>15</td>
                </tr>

                <tr>
                    <td>16</td>
                    <td>17</td>
                    <td>18</td>
                    <td>19</td>
                    <td>20</td>
                </tr>
            </table>
        </div>
        <h2 class="text-right"> أكمل بالعدد المناسب</h2>

        <div class="block-200 block">
            <b>2</b>
            <span><b>: عدد الأبقار هو  </b> </span>
        </div>

        <div class="block-200 block">
            <b>5</b>
            <span><b>: عدد الخرفان هو  </b> </span>
        </div>

        <div class="block-200 block">
            <b>2</b>
            <span><b>: عدد الأرانب هو  </b> </span>
        </div>

        <div class="block-200 block">
            <b>4</b>
            <span><b>: عدد الدجاج هو  </b> </span>
        </div>
        <br>
    </div>
@else
    <div id="active">




        <div class="animals-activity">
            <img src="{{ url('img/animals-activity.png') }}" alt="">
        </div>
        <div class="table-numbers">
            <h2 class="text-right mt-4">ما هو عدد الحيوانات في المزرعة ؟</h2>
            <h2 class="text-right">عين العدد على الجدول</h2>
            <table class="table table-hover">
                <tr>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                </tr>
                <tr>

                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                    <td>9</td>
                    <td>10</td>
                </tr>
                <tr>


                    <td>11</td>
                    <td>12</td>
                    <td>13</td>
                    <td>14</td>
                    <td>15</td>
                </tr>

                <tr>
                    <td>16</td>
                    <td>17</td>
                    <td>18</td>
                    <td>19</td>
                    <td>20</td>
                </tr>
            </table>
        </div>
        <h2 class="text-right"> أكمل بالعدد المناسب</h2>

        <div class="block-200 block">
            <input class="block-50" type="text" name="cow">
            <span><b>: عدد الأبقار هو  </b> </span>
        </div>

        <div class="block-200 block">
            <input class="block-50" type="text" name="sheep">
            <span><b>: عدد الخرفان هو  </b> </span>
        </div>

        <div class="block-200 block">
            <input class="block-50" type="text" name="t=rabbit">
            <span><b>: عدد الأرانب هو  </b> </span>
        </div>

        <div class="block-200 block">
            <input class="block-50" type="text" name="chicken">
            <span><b>: عدد الدجاج هو  </b> </span>
        </div>
        <br>
    </div>
@endif

