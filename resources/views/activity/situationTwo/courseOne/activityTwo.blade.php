


@if(session('solution'))
    <div id="active">
        <h2 class="text-right mt-5">عد الأدوات المدرسية</h2>
        <div class="animals-activity">
            <img src="{{ url('img/school-kit-activity.png') }}" alt="">
        </div>

        <div class="block-200 block"></div>

        <div class="block-300 block">
            <table class="table table-hover">
                <tr>
                    <td width="70%"><b>عدد المقصات</b> </td>
                    <td><b>4</b></td>
                </tr>
                <tr>
                    <td width="70%"><b>عدد المساطر </b></td>
                    <td><b>2</b></td>
                </tr>
                <tr>
                    <td width="70%"><b>عدد كل الأقلام</b> </td>
                    <td><b>13</b></td>
                </tr>
                <tr>
                    <td width="70%"><b>عدد كل الأدوات</b> </td>
                    <td><b>19</b></td>
                </tr>
            </table>
        </div>


        <br>
        <br>
        <br>


    </div>
@else
    <div id="active">
        <h2 class="text-right mt-5">عد الأدوات المدرسية</h2>
        <div class="animals-activity">
            <img src="{{ url('img/school-kit-activity.png') }}" alt="">
        </div>

        <div class="block-200 block"></div>

        <div class="block-300 block">
            <table class="table table-hover">
                <tr>
                    <td width="70%"><b>عدد المقصات</b> </td>
                    <td><input type="text" name="seizer" class="block-50" /></td>
                </tr>
                <tr>
                    <td width="70%"><b>عدد المساطر </b></td>
                    <td><input type="text" name="rules" class="block-50"/></td>
                </tr>
                <tr>
                    <td width="70%"><b>عدد كل الأقلام</b> </td>
                    <td><input type="text" name="pens" class="block-50"/></td>
                </tr>
                <tr>
                    <td width="70%"><b>عدد كل الأدوات</b> </td>
                    <td><input type="text" name="all" class="block-50"/></td>
                </tr>
            </table>
        </div>


        <br>
        <br>
        <br>


    </div>
@endif




