
@if(session('solution'))
    <div id="active">

        <h1 class="text-right mt-5"> أكمل العدد المناسب </h1>
        <div class="animals-activity">
            <img src="{{ url('img/marsoufa.png') }}" alt="">
        </div>
        <div class="block block-400 text-right">

            <b>2</b>
            <b>الدب في السطر </b>
            <b>4</b>
            <b>  وفي العمود</b>
            <br>

            <b>2</b>
            <b>الحصان في السطر </b>
            <b>2</b>
            <b>  وفي العمود</b>
            <br>


            <b>6</b>
            <b>الفيل في السطر </b>
            <b>1</b>
            <b>  وفي العمود</b>
            <br>

            <b>3</b>
            <b>الأسد في السطر </b>
            <b>2</b>
            <b>  وفي العمود</b>

            <br>
        </div>

        <h3 class="text-right ">

            الحيوان الموجود في السطر 3 وفي العمود 4 هو
            :
            <span class="p-3 green">القط</span>
        </h3>
    </div>
@else
    <div id="active">

        <h1 class="text-right mt-5"> أكمل العدد المناسب </h1>
        <div class="animals-activity">
            <img src="{{ url('img/marsoufa.png') }}" alt="">
        </div>
        <div class="block block-400 text-right">
            <input type="text" class="block block-50" name="doub-r" />
            <b>الدب في السطر </b>
            <input type="text" class="block block-50" name="doub-c" />
            <b>  وفي العمود</b>
            <br>
            <input type="text" class="block block-50" name="horse-r" />
            <b>الحصان في السطر </b>
            <input type="text" class="block block-50" name="horse-c" />
            <b>  وفي العمود</b>
            <br>
            <input type="text" class="block block-50" name="elephant-r" />
            <b>الفيل في السطر </b>
            <input type="text" class="block block-50" name="elephant-c" />
            <b>  وفي العمود</b>
            <br>
            <input type="text" class="block block-50" name="lion-r" />
            <b>الأسد في السطر </b>
            <input type="text" class="block block-50" name="lion-c" />
            <b>  وفي العمود</b>
            <br>
        </div>

        <h3 class="text-right ">
            <input type="text" class="block " name="response" />
            الحيوان الموجود في السطر 3 وفي العمود 4 هو
        </h3>
    </div>
@endif


