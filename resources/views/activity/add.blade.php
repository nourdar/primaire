@section('addActivity')


@if(count($course) <= 0)
    <div class="alert alert-warning">الرجاء تحديد درس لتضيف عليه نشاط</div>
@else

    <div class="container">

        @if(session('exist'))
            <div class="alert alert-danger">هذا النشاط موجود من قبل</div>
        @endif


        @if(session('success'))
            <div class="alert alert-success">تم إضافة النشاط بنجاح</div>
        @endif
        @if(session('false'))
            <div class="alert alert-danger">لم يتم إضافة النشاط بنجاح .</div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="form-horizontal text-right " method="post" action="{{ url('admin/activity/submitAdd') }}" enctype="multipart/form-data">
            @csrf
            <fieldset>

                <!-- Form Name -->
                <legend>إضافة نشاط</legend>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class=" control-label" for="selectbasic">الدرس</label>
                    <div >
                        <select id="selectbasic" name="parent_id" class="form-control">
                                <option value="{{ $course[0]['id'] }}">{{ $course[0]['title'] }}</option>
                        </select>
                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class=" control-label" for="selectbasic">نوع النشاط</label>
                    <div >
                        <select id="selectbasic" name="type" class="form-control">
                            <option value="أكتشف">أكتشف</option>
                            <option value="أنجز">أنجز</option>
                            <option value="تعلمت">تعلمت</option>
                        </select>
                    </div>
                </div>


                <!-- Textarea -->
                <div class="form-group">
                    <label  for="description">مدة النشاط ( بالدقائق )</label>
                    <div >
                        <input type="number" name="duration" />
                    </div>
                </div>
                <!-- Textarea -->
                <div class="form-group">
                    <label  for="description">وصف النشاط</label>
                    <div >
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>
                </div>

                <!-- File Button -->
                <div class="form-group">
                    <label class=" control-label" for="image">الصورة</label>
                    <div >
                        <input id="image" name="image" class="input-file" type="file">
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class=" control-label" for="submit"></label>
                    <div >
                        <button id="submit" name="submit" type="submit" class="btn btn-primary">إضافة النشاط</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
@endif


@endsection
