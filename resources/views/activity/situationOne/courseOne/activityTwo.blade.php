

@if(session('solution'))
    <div id="active">
        <br>


        <h4 class="text-right"> أنجز حسب ما يطلب منك  - </h4>

        <div class="block-300 block">
            <div class="zones">
                <div class="zone" style="background: red" ></div>
                <div class="zone" style="background: blue" ></div>
                <div class="zone" style="background: blue" ></div>
                <div class="zone" style="background: blue" ></div>
                <div class="zone" style="background: green" ></div>
                <div class="zone" style="background: green" ></div>
            </div>
        </div>

        <div class="block-300 block">
            <div class="pen">
                <img src="{{ url('img/color_pencil_red.png') }}" id="red" alt="قلم أحمر" />
            </div>
            <div class="pen">
                <img src="{{ url('img/color_pencil_blue.png') }}" id="blue" alt="قلم أزرق" />
            </div>
            <div class="pen">
                <img src="{{ url('img/color_pencil_green.png') }}" id="green" alt="قلم أخضر" />
            </div>
        </div>

        <div class="block-300 block text-right">
            <h5>لون قريصة واحدة باللون اﻷحمر - </h5>
            <h5>لون 3 قريصات باللون الأزرق - </h5>
            <h5>لون قريصتين اثنتين باللون اﻷخضر - </h5>
        </div>






        <h4 class="text-right">إربط كل بطاقة بالعدد المناسب</h4>



        <div class="row targets">

            <div class="block-50 block" id="sfive">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/five.png') }}" width="100%" height="100%">
            </div>
            <div class="block-50 block" id="stwo">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/two.png') }}" width="100%" height="100%">
            </div>
            <div class="block-50 block" id="sthree">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/three.png') }}" width="100%" height="100%">
            </div>
            <div class="block-50 block" id="sfour">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/four.png') }}" width="100%" height="100%">
            </div>
            <div class="block-50 block" id="sone">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/one.png') }}" width="100%" height="100%">
            </div>


        </div>



        <div class="row matchers mt-5">

            <div class="block-50  block circle-50 text-center" id="snOne">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>1</h1>
            </div>
            <div class="block-50  block circle-50 text-center" id="snTwo">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>2</h1>
            </div>
            <div class="block-50  block circle-50 text-center" id="snThree">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>3</h1>
            </div>
            <div class="block-50  block circle-50 text-center" id="snFour">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>4</h1>
            </div>
            <div class="block-50  block circle-50 text-center" id="snFive">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>5</h1>
            </div>





        </div>

    </div>
 @else
    <div id="active" class="exo">
        <br>


        <h4 class="text-right"> أنجز حسب ما يطلب منك  - </h4>

        <div class="block-300 block">
            <div class="zones">
                <div class="zone" id="1"></div>
                <div class="zone" id="2"></div>
                <div class="zone" id="3"></div>
                <div class="zone" id="4"></div>
                <div class="zone" id="5"></div>
                <div class="zone" id="6"></div>
            </div>
        </div>

        <div class="block-300 block">
            <div class="pen">
                <img src="{{ url('img/color_pencil_red.png') }}" id="red" alt="قلم أحمر" />
            </div>
            <div class="pen">
                <img src="{{ url('img/color_pencil_blue.png') }}" id="blue" alt="قلم أزرق" />
            </div>
            <div class="pen">
                <img src="{{ url('img/color_pencil_green.png') }}" id="green" alt="قلم أخضر" />
            </div>
        </div>

        <div class="block-300 block text-right">
            <h5>لون قريصة واحدة باللون اﻷحمر - </h5>
            <h5>لون 3 قريصات باللون الأزرق - </h5>
            <h5>لون قريصتين اثنتين باللون اﻷخضر - </h5>
        </div>






        <h4 class="text-right">إربط كل بطاقة بالعدد المناسب</h4>



        <div class="row targets">


            <div class="block-50 block" id="two">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/two.png') }}" width="100%" height="100%">
            </div>
            <div class="block-50 block" id="five">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/five.png') }}" width="100%" height="100%">
            </div>
            <div class="block-50 block" id="three">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/three.png') }}" width="100%" height="100%">
            </div>
            <div class="block-50 block" id="four">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/four.png') }}" width="100%" height="100%">
            </div>
            <div class="block-50 block" id="one">
                <input type="checkbox" class="checkbox"  name="checkthat" id="6">
                <img class="rounded" src="{{ url('img/domino/one.png') }}" width="100%" height="100%">
            </div>


        </div>



        <div class="row matchers mt-5">


            <div class="block-50  block circle-50 text-center" id="nTwo">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>2</h1>
            </div>
            <div class="block-50  block circle-50 text-center" id="nFive">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>5</h1>
            </div>
            <div class="block-50  block circle-50 text-center" id="nThree">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>3</h1>
            </div>
            <div class="block-50  block circle-50 text-center" id="nFour">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>4</h1>
            </div>
            <div class="block-50  block circle-50 text-center" id="nOne">
                <input type="checkbox" class="checkbox" name="checkthis"  id="1">
                <h1>1</h1>
            </div>





        </div>

    </div>
@endif
<div class="seperation-bottom"></div>

