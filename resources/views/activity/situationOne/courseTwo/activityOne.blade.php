@if(session('solution'))

    <div id="active">


        <div class="block block-200  mr-5" style="height: 300px">
            <div class="circle-50" style="background: purple"></div>
            <div class="circle-50" style="background: purple"></div>
            <div class="circle-50" style="background: purple"></div>
            <div class="circle-50" style="background: purple"></div>
            <div class="circle-50" style="background: purple"></div>
            <div class="circle-50" style="background: purple"></div>
            <div class="circle-50" style="background: purple"></div>
            <div class="circle-50" style="background: purple"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>

        </div>


        <div class="block block-200 ">
            <div class="khochayba orange-1" data-color="orange-1"></div>
            <div class="khochayba red" data-color="red"></div>
            <div class="khochayba green-1" data-color="green-1"></div>
            <div class="khochayba orange" data-color="orange"></div>
            <div class="khochayba aqua" data-color="aqua"></div>
            <div class="khochayba blue" data-color="blue"></div>
            <div class="khochayba purple" data-color="purple"></div>
            <div class="khochayba green" data-color="green" id="khochaybaGreen"></div>
        </div>

        <div class="block block-400-sm  mt-5">
            <h2>لون بالأخضر الإجابات الصحيحة</h2>
            <div>
                <div class="block">
                    <div class="block-50 border text-center response">لا</div>
                    <div class="block-50 border text-center response green">نعم</div>
                    <h4>القريصات أكثر من الخشيبات</h4>
                </div>
                <div class="block">
                    <div class="block-50 border text-center response green">لا</div>
                    <div class="block-50 border text-center response">نعم</div>
                    <h4>القريصات أقل من الخشيبات</h4>
                </div>

            </div>
            <h2>لون بالبنفسجي قريصات بقدر الخشيبات</h2>
        </div>


    </div>

@else
    <div id="active">


        <div class="block block-200  mr-5" style="height: 300px">
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
            <div class="circle-50"></div>
        </div>


        <div class="block block-200 ">
            <div class="khochayba orange-1" data-color="orange-1"></div>
            <div class="khochayba red" data-color="red"></div>
            <div class="khochayba green-1" data-color="green-1"></div>
            <div class="khochayba orange" data-color="orange"></div>
            <div class="khochayba aqua" data-color="aqua"></div>
            <div class="khochayba blue" data-color="blue"></div>
            <div class="khochayba purple" data-color="purple"></div>
            <div class="khochayba green" data-color="green" id="khochaybaGreen"></div>
        </div>

        <div class="block block-400-sm  mt-5">
            <h2>لون بالأخضر الإجابات الصحيحة</h2>
            <div>
                <div class="block">
                    <div class="block-50 border text-center response">لا</div>
                    <div class="block-50 border text-center response">نعم</div>
                    <h4>القريصات أكثر من الخشيبات</h4>
                </div>
                <div class="block">
                    <div class="block-50 border text-center response">لا</div>
                    <div class="block-50 border text-center response">نعم</div>
                    <h4>القريصات أقل من الخشيبات</h4>
                </div>

            </div>
            <h2>لون بالبنفسجي قريصات بقدر الخشيبات</h2>
        </div>


    </div>
@endif

