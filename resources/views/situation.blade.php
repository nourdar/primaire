<?php
use App\Http\Controllers\SituationController;
$situation = new SituationController;
$situation->get();
?>
@section('situations')
    <!-- Projects Section -->
    <section id="situations" class="projects-section bg-light ">
        <div class="container">


@foreach($situation->all as $key)
            <!-- Situation  Row -->
            <div class="row justify-content-center no-gutters mb-5 mb-lg-0 situation" >
                <div class="col-lg-6 situation-img" >
                    <img class="img-fluid" src="{{ url('img/'.$key['image']) }}" >
                </div>
                <div class="col-lg-6">
                    <div class="bg-black text-center h-100 project">
                        <div class="d-flex h-100">
                            <div class="project-text w-100 my-auto text-right {{--text-lg-left--}}">
                                <h4 class="text-white"><a href="{{ url('course/'.$key['id']) }}">{{ $key['title'] }}</a></h4>
                                <p class="mb-0 text-white-50">{{ $key['description'] }}</p>
                                <hr class="d-none d-lg-block mb-0 mr-0 ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Situation  Row -->
@endforeach
            {{--<!-- Situation Two Row -->--}}
            {{--<div class="row justify-content-center no-gutters">--}}
                {{--<div class="col-lg-6">--}}
                    {{--<img class="img-fluid" src="img/two.jpg" width="100%" height="100%" alt="">--}}
                {{--</div>--}}
                {{--<div class="col-lg-6 order-lg-first">--}}
                    {{--<div class="bg-black text-center h-100 project">--}}
                        {{--<div class="d-flex h-100">--}}
                            {{--<div class="project-text w-100 my-auto text-center text-lg-right">--}}
                                {{--<h4 class="text-white"><a href="#">الوضعية الثانية</a></h4>--}}
                                {{--<p class="mb-0 text-white-50">--}}
                                    {{--خاصة بالنشاطات الترفيهية--}}
                                    {{--تحتوي على مجموعة من النشاطات--}}
                                {{--</p>--}}
                                {{--<hr class="d-none d-lg-block mb-0 mr-0">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- End Situation Two Row -->--}}
        </div>
    </section>

@endsection
