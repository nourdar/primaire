@section('situations')
    <!-- Projects Section -->
    <section id="projects" class="projects-section bg-light ">
        <div class="container">
        @if(count($situations) > 0)

        @foreach($situations as $situation)

            <!-- Situation  Row -->
                <div class="row justify-content-center no-gutters mb-5 mb-lg-0 situation" >
                    <input type="hidden" name="situationId" value="{{ $situation['id'] }}">
                    <div class="col-lg-6 situation-img" >
                        <img class="img-fluid" src="{{ url('img/'.$situation['image']) }}" >
                    </div>
                    <div class="col-lg-6">
                        <div class="bg-black text-center h-100 project">
                            <div class="d-flex h-100">
                                <div class="project-text w-100 my-auto text-right {{--text-lg-left--}}">
                                    <h4 class="text-white">
                                            {{ $situation['title'] }}
                                    </h4>
                                    <p class="mb-0 text-white-50">
                                        {{ $situation['description'] }}
                                        @if($situation['access'] == 1)
                                            <br>
                                            <button class="btn btn-primary btn-wide"  >
                                                <a href="{{ url('course/'.$situation['id']) }}"> الدخول إلى الوضعية</a>
                                            </button>
                                    @else
                                        <br>
                                        <div class="alert alert-warning">
                                            لا يمكنك الدخول إلى هذه الوضعية حتى تكمل الوضعية السابقة
                                        </div>
                                        @endif
                                    </p>
                                    <hr class="d-none d-lg-block mb-0 mr-0 ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Situation  Row -->

            @endforeach
            @else
                <div class="alert alert-warning text-right">لا يوجد وضعيات</div>
            @endif

        </div>
    </section>

@endsection
