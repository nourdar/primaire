
{{--include blade files--}}
@include('head')
@include('nav')

@include($page)


@include('footer')




{{--sections that will appear--}}
@yield('head')
@yield('nav')

{{--seperator--}}
<div class="seperation-top "></div>
{{--end seperator--}}

@yield($section)


@yield('footer')
