@section('footer')

    <!-- Footer -->
    <footer class="bg-black small text-center text-white-50 " >
        <div class="container">
           كل الحقوق محفوظة &copy;
        </div>
        <a href="{{ url('admin') }}">لوحة التحكم</a>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ asset('js/grayscale.min.js') }}"></script>

    <script src="{{ asset('js/sketchpad.js') }}"></script>

    <script src="{{ asset('js/linemate.min.js') }}"></script>

    <script src="{{ url('js/sweetalert.min.js') }}"></script>

    <script src="{{ asset('js/courseOne/activityOne.js') }}"></script>
    <script src="{{ asset('js/courseOne/activityTwo.js') }}"></script>

@if(session('sketch'))
    <script src="{{ asset('js/courseOne/mySketch1.js') }}"></script>
@else
    <script src="{{ asset('js/courseOne/mySketch.js') }}"></script>
@endif
    </body>

    </html>

@endsection
