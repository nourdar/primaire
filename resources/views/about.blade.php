@section('about')
<!-- About Section -->
<section id="about" class="about-section text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2 class="text-white mb-4"> تطبيق المدرسة الإلكتروني</h2>
                <p class="text-white-50">

                    تطبيق المدرسة الإلكتروني يحتوي على دروس من الكتاب المدرسي .
                    وهو عبارة عن مجموعة من الدروس والنشاطات الموجهة للطفل ليخرج بأكبر حصيلى معرفية
                </p>
            </div>
        </div>
        <img src="img/ipad.jpg" class="img-fluid" alt="">
    </div>
</section>
@endsection
