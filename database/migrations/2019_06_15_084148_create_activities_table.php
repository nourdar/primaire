<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity', function (Blueprint $table) {
            $table->increments('id');
            /**
            el darss li tab3a lih
             *
             */
            $table->integer('parent_id');

            $table->string('type');
            $table->string('image');
            $table->string('description');
            $table->integer('duration');

            /*
            * 0 if not
            * 1 if done
            * */
            $table->integer('is_done');
            $table->integer('access')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity', function (Blueprint $table) {
            //
        });
    }
}
