<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course', function (Blueprint $table) {
            $table->increments('id');
            /**
            el wad3ya li tab3a lih
             *
             */
            $table->integer('parent_id');
            $table->string('title');
            $table->string('image');

            $table->string('description');
            /*
             * 0 if not
             * 1 if done
             * */
            $table->integer('is_done');
            $table->integer('access')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course', function (Blueprint $table) {
            //
        });
    }
}
