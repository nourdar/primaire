<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "course";

    public  function situation(){
        return $this->belongsTo('App\Situation', 'parent_id');
    }
}
