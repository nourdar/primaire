<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Situation extends Model
{
    protected $table = "situation";

    public function course(){
    return $this->hasMany('App/Course','parent_id','id');
}
}
