<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Course;
use App\Situation;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    public function submitActivity(Request $request){

        if(session('step')){
            if(session('step') >= 100) {
                session(['step' => 0]);
            }
                $step = session('step')+5;
                session(['step' => $step]);

            $progress = $step."%";
        } else {
            $step = 5;
            $progress = $step."%";
            session(['step' => $step]);
        }
       session()->flash('solution', 'active');

       session([
           'progress' => $progress
       ]);
        if($request->id % 3 == 0) {

            Course::where('id', ($request->parent_id + 1))->update(['access' => '1']);
        }
        if($request->id % 6 == 0) {
            $sid =   Course::where('id', ($request->parent_id))->get();
            Situation::where('id', ($sid[0]['parent_id'] + 1))->update(['access' => '1']);
        }
       Activity::where('id', ($request->id + 1))->update(['access'=> '1']);

       return back();
    }


    public function add($parent_id){
        $course = Course::where('id', $parent_id)->get();
        $data = [
            'page'      =>  'admin.activity.add',
            'section'   =>  'addActivity',
            'course'    =>  $course,
        ];


        return view('admin.activity.activity',$data);
    }

    public function showList($parent_id){
        $activities = Activity::where('parent_id', $parent_id)->get();
        $course = Course::where('id', $parent_id)->first();
        $data = [
            'course'        => $course,
            'activities'    =>  $activities,
            'page'          =>  'activity.show',
            'section'       =>  'activitiesList',
            'showBottom'    => false
        ];

        return view('activity.activity',$data);
    }

    public function showActivity($id){
        $index = [

            // id   => page

            1   =>  "SituationOne.CourseOne.ActivityOne" ,
            2   =>  "SituationOne.CourseOne.ActivityTwo" ,
            3   =>  "SituationOne.CourseOne.ActivityThree",


            4   =>  "SituationOne.CourseTwo.ActivityOne",
            5   =>  "SituationOne.CourseTwo.ActivityTwo",
            6   =>  "SituationOne.CourseTwo.ActivityThree",


            7   =>  "SituationTwo.CourseOne.ActivityOne",
            8   =>  "SituationTwo.CourseOne.ActivityTwo",
            9   =>  "SituationTwo.CourseOne.ActivityThree",


            10   =>  "SituationTwo.CourseTwo.ActivityOne",
            11   =>  "SituationTwo.CourseTwo.ActivityTwo",
            12   =>  "SituationTwo.CourseTwo.ActivityThree",


            13   =>  "SituationThree.CourseOne.ActivityOne",
            14   =>  "SituationThree.CourseOne.ActivityTwo",
            15   =>  "SituationThree.CourseOne.ActivityThree",



            16   =>  "SituationThree.CourseTwo.ActivityOne",
            17   =>  "SituationThree.CourseTwo.ActivityTwo",
            18   =>  "SituationThree.CourseTwo.ActivityThree",



        ];


        $activity = Activity::where('id', $id)->get();
        if(count($activity) > 0 ) {
            $next = $activity[0]['id'] + 1 ;



            $data = [
                'page'      => 'activity.'.$index[$id],
                'section'    => 'activity',
                'activity'  => $activity[0],
            ];
        } else {

            $data = [
                'page'      => 'activity.empty',
                'section'    => 'empty',

            ];
            return view('activity.activity', $data);
        }

        return view('activity.activity', $data);
    }

    public function edit($id){
        $activity = Activity::where('id', $id)->get();

        $course = Course::where('id', $activity[0]['parent_id'])->get();
        $data = [
            'page'          => 'admin.activity.edit',
            'section'       => 'editActivity',
            'activity'      => $activity[0],
            'course'        => $course[0]
        ];
        return view('admin.activity.activity',$data);
    }

    public function update(Request $request){

        $data = [
            'type'           => $request->type,
            'description'    => $request->description,
            'duration'       => $request->duration,
            'is_done'         => $request->is_done,
        ];
        //        uploading image
        $img = $request->file('image');

        if($img) {
            $fileName = rand(1,100).rand(1,1000);

            $img->move('img',$fileName);
            $data['image'] = $fileName;
        }



        $update = Activity::where('id', $request->id)->update($data);

        if($update) {
            $request->session()->flash('success');
            return back();
        } else {
            $request->session()->flash('false');
            return back();
        }
    }

    public function adminShow($parent_id){
        $activities = Activity::where('parent_id', $parent_id)->get();
        $data = [
            'activities'    =>  $activities,
            'page'          =>  'admin.activity.show',
            'section'       =>  'activitiesList',
        ];

        return view('admin.activity.activity',$data);
    }

    public function submitAdd(Request $data){
        $types = Activity::where('parent_id', $data->parent_id)->get();

        foreach ($types as $type) {
            if($type->type  == $data->type) {
                $data->session()->flash('exist');
                return back();
            }
        }


        $this->validate($data,[
            'duration' => 'required',
            'image' => 'required'
        ]);
//        uploading image
        $img = $data->file('image');
        $fileName = rand(1,100).rand(1,1000);
        $img->move('img',$fileName);

        $insert = [
            'parent_id'     => $data->parent_id,
            'type'          => $data->type,
            'duration'      => $data->duration,
            'description'   => $data->description,
            'is_done'       => 0,
            'image'         => $fileName
        ];

        if(Activity::insert($insert)) {
            $data->session()->flash('success');
            return back();
        } else {
            $data->session()->flash('false');
            return back();
        }


    }

    public function delete(Request $request){

        $id = $request->id;
        $delete = Activity::where('id', $id)->delete();
        if($delete) {
            $request->session()->flash('success');
            return back();
        } else {
            $request->session()->flash('false');
            return back();
        }

    }
    
}
