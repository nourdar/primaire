<?php

namespace App\Http\Controllers;

use App\Situation;
use Illuminate\Http\Request;


class SituationController extends Controller
{


    public function show(){
        $situations = Situation::all();
        $data= [
            'situations'    => $situations,
            'page'          => 'situation.show',
            'section'       => 'situations'
        ];
        return view('situation.situation', $data);
    }

    public function adminShow(){
        $situations = Situation::all();
        $data = [
            'page'       => 'admin.situation.show',
            'section'    => 'situations',
            'situations' => $situations
        ];
        return view('admin.situation.situation',$data);
    }

    public function add(){
        $data = [
            'page'      =>  'admin.situation.add',
            'section'   =>  'addSituation',
        ];
        return view('admin.situation.situation',$data);
    }

    public function edit($id){
        $situation = Situation::where('id', $id)->get();
        $data = [
            'page'       => 'admin.situation.edit',
            'section'    => 'editSituation',
            'situation'  => $situation[0]
        ];
        return view('admin.situation.situation',$data);
    }

    public function update(Request $request){

        $data = [
            'title'         => $request->title,
            'description'   => $request->description,
            'is_done'       => $request->is_done,
        ];
        //        uploading image
        $img = $request->file('image');

        if($img) {
            $fileName = rand(1,100).rand(1,1000);

            $img->move('img',$fileName);
            $data['image'] = $fileName;
        }



        $update = Situation::where('id', $request->id)->update($data);

        if($update) {
            $request->session()->flash('success');
            return back();
        } else {
            $request->session()->flash('false');
            return back();
        }
    }

    public function submitAdd(Request $data){

        $this->validate($data,[
            'title' => 'required',
            'image' => 'required'
        ]);
//        uploading image
        $img = $data->file('image');
        $fileName = rand(1,100).rand(1,1000);
        $img->move('img',$fileName);


        $insert = [
            'title'         => $data->title,
            'description'   => $data->description,
            'is_done'       => 0,
            'image'         => $fileName
        ];

        if(Situation::insert($insert)) {
            $data->session()->flash('success');
            return back();
        } else {
            $data->session()->flash('false');
            return back();
        }



    }

    public function delete(Request $request){
        $id = $request->id;
        $delete = Situation::where('id', $id)->delete();
        if($delete) {
            $request->session()->flash('success');
            return back();
        } else {
            $request->session()->flash('false');
            return back();
        }

    }

}
