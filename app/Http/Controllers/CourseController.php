<?php

namespace App\Http\Controllers;

use App\Course;
use App\Situation;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index($id){
        $courses = Course::where('parent_id', $id)->get();
        $situation = Situation::where('id', $id)->first();

        $data = [
            'situation'=> $situation,
            'courses'  => $courses,
            'page'     =>  'course.courseList',
            'section'  =>  'courseList',
        ];

        return view('course.course',$data);
    }

    public function add($id){
        $situations = Situation::all();
        $current = Situation::where('id', $id)->get();
        $data = [
            'page'      =>  'admin.course.add',
            'section'   =>  'addCourse',
            'current'   =>  $current[0],
            'situations' =>  $situations
        ];
        return view('admin.course.course',$data);
    }

    public function edit($id){
        $course = Course::where('id', $id)->get();
        $situation = Situation::all();
        $data = [
            'page'          => 'admin.course.edit',
            'section'       => 'editCourse',
            'course'        => $course[0],
            'situation'     => $situation
        ];
        return view('admin.course.course',$data);
    }

    public function update(Request $request){

        $data = [
            'parent_id'     => $request->parent_id,
            'title'         => $request->title,
            'description'   => $request->description,
            'is_done'       => $request->is_done,
        ];
        //        uploading image
        $img = $request->file('image');

        if($img) {
            $fileName = rand(1,100).rand(1,1000);

            $img->move('img',$fileName);
            $data['image'] = $fileName;
        }



        $update = Course::where('id', $request->id)->update($data);

        if($update) {
            $request->session()->flash('success');
            return back();
        } else {
            $request->session()->flash('false');
            return back();
        }
    }

    public function adminShow($id){
        $courses = Course::where('parent_id', $id)->get();
        $data = [
            'courses'  => $courses,
            'page'     =>  'admin.course.show',
            'section'  =>  'courseList',
        ];

        return view('admin.course.course',$data);
    }

    public function submitAdd(Request $data){
        $this->validate($data,[
            'title' => 'required',
            'image' => 'required'
        ]);
//        uploading image
        $img = $data->file('image');
        $fileName = rand(1,100).rand(1,1000);
        $img->move('img',$fileName);

        $insert = [
            'parent_id'     => $data->parent_id,
            'title'         => $data->title,
            'description'   => $data->description,
            'is_done'       => 0,
            'image'         => $fileName
        ];

        if(Course::insert($insert)) {
            $data->session()->flash('success');
            return back();
        } else {
            $data->session()->flash('false');
            return back();
        }


    }


    public function delete(Request $request){
        $id = $request->id;
        $delete = Course::where('id', $id)->delete();
        if($delete) {
            $request->session()->flash('success');
            return back();
        } else {
            $request->session()->flash('false');
            return back();
        }

    }
}
