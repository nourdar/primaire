<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



//Admin Panel Routes

Route::prefix('admin')->group(function(){

    Route::get('/','AdminController@index');

    Route::get('course/add/{id}','CourseController@add');
    Route::get('course/edit/{id}','CourseController@edit');
    Route::get('course/{id}','CourseController@adminShow');
    Route::post('course/edit/update','CourseController@update');
    Route::post('course/submitAdd','CourseController@submitAdd');
    Route::post('course/delete','CourseController@delete');

    Route::get('situation/add','SituationController@add');
    Route::get('situation/edit/{id}','SituationController@edit');
    Route::get('situation/show','SituationController@adminShow');
    Route::post('situation/update','SituationController@update');
    Route::post('situation/submitAdd','SituationController@submitAdd');
    Route::post('situation/delete','SituationController@delete');

    Route::get('activity/show/{id}', 'ActivityController@adminShow');
    Route::get('activity/add/{parent_id}', 'ActivityController@add');
    Route::get('activity/edit/{id}', 'ActivityController@edit');
    Route::post('activity/edit/update', 'ActivityController@update');
    Route::post('activity/submitAdd','ActivityController@submitAdd');
    Route::post('activity/delete','ActivityController@delete');


});

// Situations Routes
Route::get('situation/show','SituationController@show');


// Courses Routes

Route::get('course/{id}','CourseController@index');

//Activities Routes

Route::prefix('activity')->group(function(){

    Route::get('show/{id}','ActivityController@showList');
    Route::get('/{id}','ActivityController@showActivity');
    Route::post('post/submit','ActivityController@submitActivity');


});
