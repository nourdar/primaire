
var timing  = true,
    id,
    target  = ['one', 'two', 'three', 'four', 'five'],
    matcher = ['nOne', 'nTwo', 'nThree', 'nFour', 'nFive'];

    var img = false;

var items = ['#ff3300', '#242424', '#ccc',  'green', 'purple'];


var branchOption1 = {
    color: "red",
    path: 'square-v',
    width: 5
};
var branchOption2 = {
    color: "blue",
    path: 'square-v',
    width: 5
};
var branchOption3 = {
    color: "#b437ff",
    path: 'square-v',
    width: 5
};
var branchOption4 = {
    color: "black",
    path: 'square-v',
    width: 5
};
var branchOption5 = {
    color: "tomato",
    path: 'square-v',
    width: 5
};
var branchOption6 = {
    color: "pink",
    path: 'square-v',
    width: 5
};
var branchOption7 = {
    color: "purple",
    path: 'square-v',
    width: 5
};
var branchOption8 = {
    color: "brown",
    path: 'square-v',
    width: 5
};
var branchOption9 = {
    color: "green",
    path: 'square-v',
    width: 5
};
var branchOption10 = {
    color: "yellow",
    path: 'square-v',
    width: 5
};

$(document).ready(function () {

    linemate.connect(['#sone', '#snOne'],branchOption1);
    linemate.connect(['#stwo', '#snTwo'],branchOption2);
    linemate.connect(['#sthree', '#snThree'],branchOption3);
    linemate.connect(['#sfour', '#snFour'],branchOption4);
    linemate.connect(['#sfive', '#snFive'],branchOption5);

    linemate.connect(['#', '#'],branchOption6);
    linemate.connect(['#', '#'],branchOption7);
    linemate.connect(['#', '#'],branchOption8);
    linemate.connect(['#', '#'],branchOption9);
    linemate.connect(['#', '#'],branchOption10);

});


get_random = function (list) {
    return list[Math.floor((Math.random()*list.length))];
}



function startTimer(duration, display) {
    var timer = duration, minutes, seconds;

    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = 0;
            timing = false;
            return "time end";
        }
    }, 1000);
}

window.onload = function () {


 
    var time = $('#time').attr('data-time');

    var delay = 60 * time;
        display = document.querySelector('#time-display');
    startTimer(delay, display);

};

var timerInterval = setInterval(function () {
    if(timing == false) {


        $('#active').fadeOut(1000);
        // $('.targets').fadeOut(1300);

    }
    timing = true;
},500);

linemate.confine('#activity');
linemate.defaults({ color: '#eee' });





$('.activity .block-1').on('click', function () {


    if($(this).hasClass('selected-block')) {
        $(this).removeClass('selected-block');


    } else {
        $(this).addClass('selected-block');
    }
});


$('.targets .block-1').on('click', function () {
    img = true;
    id = $(this).attr('id');
    target['id'] = id;
});


$('.matchers .block-1').on('click', function () {
    if(img === false) {
        swal({
            title: "مهلا!",
            text: "عليك أن تظغط على الصورة أولا!",
            icon: "info",
        });
    }
    mId = $(this).attr('id');

console.log(matcher[target.id]);

    var item = get_random(items);
    var branchOptions = {
        color: item,
        path: 'square-v',
        width: 5
    };

    if(matcher[target.id] === undefined ) {
        matcher[target.id] = mId;
        linemate.connect(['#'+mId, '#'+target.id],branchOptions);
        img = false;
    }


});



// course Two
var greenPicked = false;
var redPicked = false;

$(".khochayba").on('click', function () {
    if($(this).attr('data-color') == "green") {
        greenPicked =  true;
        console.log('picked');
    } else {
        greenPicked = false;
    }

});

$('.response').on('click',function(){
    if(greenPicked === true) {
        if($(this).hasClass('selectedResponse')){
            $(this).removeClass('selectedResponse');
            $(this).attr('data-selected', '');
        } else {
            $(this).addClass('selectedResponse');
            $(this).attr('data-selected', 'yes');
        }
    } else {
        swal('عليك أن تقوم بإختيار الخشيبة الخضراء');
    }
});


$(".khochayba").on('click', function () {
    if($(this).attr('data-color') == "red") {
        redPicked =  true;
        console.log('picked');
    } else {
        redPicked = false;
    }

});

$('.response-1').on('click',function(){
    if(redPicked === true) {
        if($(this).hasClass('selectedResponse-red')){
            $(this).removeClass('selectedResponse-red');
            $(this).attr('data-selected', '');
        } else {
            $(this).addClass('selectedResponse-red');
            $(this).attr('data-selected', 'yes');
        }
    } else {
        swal('عليك أن تقوم بإختيار اللون الأحمر');
    }
});


$('td').on('click',function(){
        $('td').removeClass('selectedResponse-red');
        $(this).addClass('selectedResponse-red');
        $(this).attr('data-selected', 'yes');
    });


$('.fendj').on('click',function(){
    if($(this).hasClass('selectedResponse-red')){
        $(this).removeClass('selectedResponse-red');
        $(this).attr('data-selected', '');
    }
    else {
        $(this).addClass('selectedResponse-red');
        $(this).attr('data-selected', 'yes');
    }
});







$('#circleBlue').on('click', function () {
    $(this).css('background', 'white');
});

$('#circleBrown').on('click', function () {
    $(this).css('background', 'white');
});



$('#reload').on('click',function (e) {
   e.preventDefault();
    window.location.reload();
});



